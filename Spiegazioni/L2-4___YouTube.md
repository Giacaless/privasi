# ~~YouTube~~ Video in un riflesso

> C'è un modo per evitare la profilazione di YouTube? E quant'è cambiata la piattaforma in questi anni? Alla scoperta dei *mirror*

## In parole semplici

Per questo capitolo adotteremo un approccio diverso: vi abbiamo già raccontato pienamente di come funzioni Google (e YouTube appartiene a Google dal 2006), quindi dibattere per l'ennesima volta sulla privacy e i suoi modi per fare soldi ci pare superfluo. Parliamo, invece, semplicemente di YouTube.  

Il sito fu creato nel 2005 da tre impiegati PayPal e quello che offriva era semplice: uno spazio per condividere video. Non c'erano i pollici per votare, bensì le stelline (da 1 a 5), la gente si arrangiava con ciò che aveva in casa e fare lo "YouTuber" come lavoro non era neanche immaginabile. La gente lo faceva per il gusto di fare, per esprimersi, per mettersi in contatto con altre persone, ma ancora di più per costruire una realtà più "personale" che si distaccasse dalla TV.  

Con il passare del tempo, YouTube è inevitabilmente cambiato. Sono emersi i manager, fino a che YouTube non è diventato in alcuni casi una piattaforma di lancio per finire in TV o sul grande schermo¹. E va bene, questo alla fine non è dipeso dalla piattaforma.  
Quello che invece *è* dipeso dalla piattaforma, è come YouTube in questi ultimi anni abbia iniziato a prediligere i canali di emittenti televisive, scordandosi dell'utenza che lo aveva reso ciò che era. Sono noti più casi dove la piattaforma ha trattato con due pesi due misure le emittenti e gli YouTuber che parlavano del medesimo argomento, demonetizzando questi ultimi: per esempio quando il conduttore del Jimmy Kimmel Live (show televisivo della ABC) parlò delle sparatoria avvenuta a Las Vegas nel 2017 andò tutto bene², ma quando ne parlò lo YouTuber Casey Neistat aprendo addirittura un fondo per le vittime, venne privato da YouTube della possibilità di guadagnare soldi su quel video³.  

Ulteriore testimonianza di questo risvolto sono gli ultimi YouTube Rewind: gli YouTube Rewind sono video che dovrebbero riassumere gli eventi clou successi in un anno sulla piattaforma. La community si è sentita sempre meno rappresentata dai Rewind, dove sono apparsi personaggi della TV come Will Smith, John Oliver e Trevor Noah⁴. Inoltre, come evidenziato dagli YouTuber RackaRacka, YouTube è diventato sì "disneyano", ma al tempo stesso produce show come Wayne che vanno contro a tutte le policy che tanto vuol fare rispettare a suon di demonetizzazione⁵ (scene violente ed esplicite, imprecazioni continue).

E quest'arma che YouTube usa, la demonetizzazione (ovvero non permettere di guadagnare soldi su un determinato video), è diventata un incubo per molti YouTuber che hanno timore di parlare di certi argomenti per paura di farla scattare. La cosa più grave è che, una volta demonetizzato un video *o addirittura l'intero canale*, YouTube non dà mai una motivazione precisa se non che "ha violato gli standard della community". È come venire accusati di un reato ma senza dirvi quale.  

Insomma: le cose cambiano, certo, ma nel corso degli anni YouTube è diventato esattamente quell'ambiente dal quale si voleva scappare: la TV e la sua censura. Che collabora con la TV e che produce i suoi show originali, trattando con poco riguardo (e ingiustizia) quei creatori che hanno contribuito a far diventare popolare la piattaforma. Quest'ultimi, per arrangiarsi hanno aperto donazioni su siti esterni proprio per evitare le ansie della demonetizzazione, come Patreon o Ko-Fi. Ed è infatti diventato più efficiente supportarli da lì che con una visualizzazione.  

Cos'è quindi più etico fare? Venire profilati e contribuire a un sistema che non ricompensa neanche più i suoi creatori o continuarne egoisticamente ad usufruirne evitando sia la profilazione sia il supportare queste persone che faticano sempre di più? La risposta più sensata sarebbe in verità costruire delle alternative per far sì che questi creatori abbandonino YouTube, ma di questo ne parleremo nel dettaglio in "Tecnicamente".  

Quello che possiamo invece fare già da oggi e senza il minimo sforzo è guardare i video di YouTube da dei *mirror* (letteralmente "specchi"). Questi servizi (siti o app) sono servizi non ufficiali che spogliano i video da ogni mezzo di profilazione. E se vogliamo davvero supportare il nostro YouTuber preferito, possiamo prendere in considerazione l'idea di donargli soldi su uno di quei siti esterni elencati prima (ovviamente SE usa uno di questi siti).  

## Cosa fare

[DA AGGIUNGERE: invidious, FreeTube, NewPipe]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente

[DA AGGIUNGERE: discorso etico e alternative]  

## Approfondendo

[DA AGGIUNGERE]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ come lo spiacevole [Game Therapy](https://www.imdb.com/title/tt4421344/)  
² Jimmy Kimmel Live, [Jimmy Kimmel on Mass Shooting in Las Vegas](https://invidio.us/watch?v=ruYeBXudsds) (video), 2017  
³ CaseyNeistat, [LET'S HELP THE VICTIMS OF THE LAS VEGAS ATTACK](https://invidio.us/watch?v=tfZvSS_f254) (video), 2017  
⁴ YouTube, [YouTube Rewind 2018: Everyone Controls Rewind | #YouTubeRewind](https://invidio.us/watch?v=YbJOTdZBX1g) (video), 2018  
⁵ RackaRacka, [Youtube hates RackaRacka #freeracka](https://invidio.us/watch?v=q3blsxaw6bg) (video), 2019  